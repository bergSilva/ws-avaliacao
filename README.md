# ws-avaliação
# Requisitos do Sistema
   
   - Spring Boot - versão 2.1.7
   - MYSQL 8.0.17 MySQL Community Server - GPL
   
   

## Base de dados
 - Caso MySQL Instalado , executar o comando sql;
 
        create database avalicaodb;
    
 - Caso nao tenha MYSQL instalado executar o scrip docker abaixo  para subir a  base de dados;
           
            docker pull mysql  
 
            docker run -e MYSQL_ALLOW_EMPTY_PASSWORD=true -e MYSQL_DATABASE =avaliacaodb -p 3306:3306 -d mysql 
 
    A entidade `estado` foi adicionada para atender a normalização do banco;
 
    Obs.: É inserido todos os estados ao execucar o comando `run` citado no tópito Projeto, através do migration da biblioteca FlyWay.
 
        `<dependency>
            <groupId>org.flywaydb</groupId>
            <artifactId>flyway-maven-plugin</artifactId>
            <version>5.2.4</version>
        </dependency>`
 

## Projeto (executar com maven)

   Através do terminal do Intellij

    - mvn clean spring-boot:run 

# Teste API (Swagger) 

- localhost:8080/swagger-ui.html

 As consultas solicitadas estão no endponit `List all`


       

