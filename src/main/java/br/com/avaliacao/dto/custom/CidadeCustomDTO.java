package br.com.avaliacao.dto.custom;

import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CidadeCustomDTO {

    private Long  id;

    private String nome;

}
