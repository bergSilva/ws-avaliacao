package br.com.avaliacao.dto;

import br.com.avaliacao.dto.custom.CidadeCustomDTO;
import br.com.avaliacao.enumeration.SexoEnum;
import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ClienteDTO implements Serializable {

    private Long id;

    private String nomeCompleto;

    private SexoEnum sexo;

    private String dataNasc;

    private Integer idade;

    private CidadeCustomDTO cidade;

}
