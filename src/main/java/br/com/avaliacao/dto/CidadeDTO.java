package br.com.avaliacao.dto;

import br.com.avaliacao.model.Estado;
import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CidadeDTO {

    private Long  id;

    private String nome;

    private Estado estado;

}
