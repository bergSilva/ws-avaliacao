package br.com.avaliacao.dto.filter;

import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CidadeDTOFilter {

    private String nome;

    private String estadoNome;

}
