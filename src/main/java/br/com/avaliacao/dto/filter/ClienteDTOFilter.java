package br.com.avaliacao.dto.filter;

import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ClienteDTOFilter {

    private Long id;

    private String nomeCompleto;


}
