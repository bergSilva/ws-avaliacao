package br.com.avaliacao.service;



import br.com.avaliacao.dto.ClienteDTO;
import br.com.avaliacao.dto.filter.ClienteDTOFilter;
import br.com.avaliacao.model.Cliente;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
@Service
public interface ClienteService extends GenericService<Cliente, Long> {

    Page<ClienteDTO> listarTodosPaginado(Pageable pageable, ClienteDTOFilter filtroDTO);
}
