package br.com.avaliacao.service;


import br.com.avaliacao.dto.CidadeDTO;
import br.com.avaliacao.dto.filter.CidadeDTOFilter;
import br.com.avaliacao.model.Cidade;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public interface CidadeService extends GenericService<Cidade, Long> {
    Page<CidadeDTO> listarTodosPaginado(Pageable pageable, CidadeDTOFilter filtroDTO);

}
