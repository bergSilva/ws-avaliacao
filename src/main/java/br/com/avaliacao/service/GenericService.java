package br.com.avaliacao.service;

import br.com.avaliacao.model.AbstractEntity;
import org.eclipse.jdt.core.compiler.InvalidInputException;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 * Generic Service para ser utilizado pelas entidades
 *
 * @param <T>
 * @param <I>
 */
@Service
public interface GenericService<T extends AbstractEntity<I>, I> {

    T saveOrUpdate(T entity) throws InvalidInputException;

    Page<T> getAllPaginated(Example<T> example, Pageable pageable);

    T get(I id);

    T add(T entity);

    T update(T entity);

    void remove(I id);


}
