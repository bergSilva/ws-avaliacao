package br.com.avaliacao.service.impl;


import br.com.avaliacao.controller.exception.NotFoundException;
import br.com.avaliacao.dto.ClienteDTO;
import br.com.avaliacao.dto.custom.CidadeCustomDTO;
import br.com.avaliacao.dto.filter.ClienteDTOFilter;
import br.com.avaliacao.mapper.ClienteMapper;
import br.com.avaliacao.model.Cliente;
import br.com.avaliacao.repository.ClientRepository;
import br.com.avaliacao.service.ClienteService;
import br.com.avaliacao.util.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClienteServiceImpl  implements ClienteService {


    @Autowired
    private ClientRepository clientRepository;

    @Override
    public Cliente saveOrUpdate(Cliente entity) {
        return this.clientRepository.save(entity);
    }

    @Override
    public Page<Cliente> getAllPaginated(Example<Cliente> example, Pageable pageable) {
      return clientRepository.findAll(example,pageable);
    }

    @Override
    public Cliente get(Long id) {
        Optional<Cliente> optionalCliente =  clientRepository.findById(id);

       if(!optionalCliente.isPresent())
           throw  new NotFoundException("Entidade nao encotrada");
        return optionalCliente.get();
    }

    @Override
    public Cliente add(Cliente entity) {
        return this.saveOrUpdate(entity);
    }

    @Override
    public Cliente update(Cliente entity) {
        Cliente cliente = entity;
        return this.saveOrUpdate(entity);
    }

    @Override
    public void remove(Long id) {
        Cliente cliente = this.get(id);
        this.clientRepository.delete(cliente);
    }

    public  ClienteDTO convertToDTO(Cliente entity) {
        Integer  idade= DateUtils.getIdade(entity.getDataNasc());
        return ClienteDTO.builder()
                .id(entity.getId())
                .nomeCompleto(entity.getNomeCompleto())
                .sexo(entity.getSexo())
                .cidade(CidadeCustomDTO.builder()
                        .id(entity.getCidade().getId())
                         .nome(entity.getCidade().getNome())
                        .build())
                .idade(DateUtils.getIdade(entity.getDataNasc()))
                .dataNasc(DateUtils.converteDateToString(entity.getDataNasc()))
                .build();
    }

    @Override
    public Page<ClienteDTO> listarTodosPaginado(Pageable pageable, ClienteDTOFilter filtroDTO) {
        Page<Cliente> clienteDTOPage;
        Cliente cliente =ClienteMapper.convertFilterToEntity(filtroDTO);
        ExampleMatcher matcher = ExampleMatcher.matching()
                .withStringMatcher(ExampleMatcher.StringMatcher.CONTAINING)
                .withIgnoreCase();
        Example<Cliente> example = Example.of(cliente,matcher);
        clienteDTOPage = getAllPaginated(example,pageable);

        return clienteDTOPage.map(this::convertToDTO) ;
    }
}
