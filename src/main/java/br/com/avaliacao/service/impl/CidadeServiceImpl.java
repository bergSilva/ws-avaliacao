package br.com.avaliacao.service.impl;


import br.com.avaliacao.controller.exception.NotFoundException;
import br.com.avaliacao.dto.CidadeDTO;
import br.com.avaliacao.dto.filter.CidadeDTOFilter;
import br.com.avaliacao.mapper.CidadeMapper;

import br.com.avaliacao.model.Cidade;

import br.com.avaliacao.repository.CidadeRepository;
import br.com.avaliacao.service.CidadeService;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CidadeServiceImpl implements CidadeService {

    @Autowired
    private CidadeRepository cidadeRepository;

    @Override
    public Cidade saveOrUpdate(Cidade entity) {
        return cidadeRepository.save(entity);
    }

    @Override
    public Page<Cidade> getAllPaginated(Example<Cidade> example, Pageable pageable) {
        return  cidadeRepository.findAll(example,pageable);
    }

    @Override
    public Cidade get(Long id) {

        Optional<Cidade> optionalCidade = cidadeRepository.findById(id);

        if(!optionalCidade.isPresent())
            throw  new NotFoundException("entidade nao encotrada");
        return optionalCidade.get();
    }

    @Override
    public Cidade add(Cidade entity)  {
            return this.saveOrUpdate(entity);
    }

    @Override
    public Cidade update(Cidade entity) {
        return this.saveOrUpdate(entity);
    }

    @Override
    public void remove(Long  id) {
        Cidade cidade = this.get(id);
        this.cidadeRepository.delete(cidade);
    }

    @Override
    public Page<CidadeDTO> listarTodosPaginado(Pageable pageable, CidadeDTOFilter filtroDTO) {
        Page<Cidade> cidadePage;
        Cidade cidade = CidadeMapper.convertFilterToEntity(filtroDTO);
        ExampleMatcher matcher = ExampleMatcher.matching()
                .withStringMatcher(ExampleMatcher.StringMatcher.CONTAINING)
                .withIgnoreCase();
        Example<Cidade> example = Example.of(cidade,matcher);
        cidadePage = getAllPaginated(example,pageable);

        return cidadePage.map(this::convertToDTO) ;
    }

    public CidadeDTO convertToDTO(Cidade entity) {
        return CidadeDTO.builder()
                .id(entity.getId())
                .nome(entity.getNome())
                .estado(entity.getEstado()).build();
    }
}
