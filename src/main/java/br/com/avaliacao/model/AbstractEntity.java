package br.com.avaliacao.model;

import java.io.Serializable;

@SuppressWarnings("squid:S00119")
public interface AbstractEntity<ID> extends Serializable {
    ID getId();
    void  setId(ID id);
}
