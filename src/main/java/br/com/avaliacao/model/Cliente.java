package br.com.avaliacao.model;

import br.com.avaliacao.enumeration.SexoEnum;
import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Builder
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name ="cliente")
public class Cliente  implements  AbstractEntity<Long> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String nomeCompleto;

    private SexoEnum sexo;

    @Temporal(TemporalType.DATE)
    private Date dataNasc;

    @Transient
    private Integer idade;

    @ManyToOne
    @JoinColumn(name = "cidade_id")
    private Cidade cidade;


}
