package br.com.avaliacao.controller;

import br.com.avaliacao.anotations.DefaultParamsPagedList;
import br.com.avaliacao.dto.CidadeDTO;
import br.com.avaliacao.dto.filter.CidadeDTOFilter;
import br.com.avaliacao.mapper.CidadeMapper;
import br.com.avaliacao.model.Cidade;
import br.com.avaliacao.service.CidadeService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


@RestController
@RequestMapping("/cidade")
public class CidadeController{

    @Autowired
    private CidadeService service;
    private CidadeMapper mapper;

    public CidadeController() {
        this.mapper = new CidadeMapper();
    }

    @GetMapping
    @ApiOperation(value = "List all")
    @DefaultParamsPagedList
    Page<CidadeDTO> getAll(Integer page, Integer count, String order, String sortProperty, CidadeDTOFilter cidadeDTOFilter) {
        PageRequest pageRequest = PagingUtils.createPageRequest(page, count, order, sortProperty);
        Page<CidadeDTO> pages = service.listarTodosPaginado(pageRequest,cidadeDTOFilter);
        return pages;
    }

    @GetMapping("/findById")
    public CidadeDTO findById(@RequestParam( "cidadeId") Long  cidadeId) {
        return mapper.convertToDTO(service.get(cidadeId));
    }

    @PostMapping("/createCidade")
    @ApiOperation("Inserir cidade")
    public CidadeDTO create(@ApiParam(value = "cidadeDTO", required = true)
                                                 @RequestBody @Valid CidadeDTO cidadeDTO) {
        Cidade cidade = mapper.convertToEntity(cidadeDTO);
        return this.mapper.convertToDTO(this.service.add(cidade));
    }

    @PutMapping
    @ApiOperation("Atualizar Cidade")
    public CidadeDTO update(@ApiParam(value = "cidadeDTO", required = true)
                                  @RequestBody @Valid  CidadeDTO cidadeDTO) {
        Cidade cidade = mapper.convertToEntity(cidadeDTO);
        return this.mapper.convertToDTO(this.service.update(cidade));
    }
    @DeleteMapping("/{id}")
    @ApiOperation("Deletar cidade")
    public void delete(@ApiParam(value = "id", required = true) @PathVariable  Long id) {
        service.remove(id);
    }


}
