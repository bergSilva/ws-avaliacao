package br.com.avaliacao.controller;


import br.com.avaliacao.anotations.DefaultParamsPagedList;
import br.com.avaliacao.dto.filter.ClienteDTOFilter;
import br.com.avaliacao.mapper.ClienteMapper;
import br.com.avaliacao.model.Cliente;
import br.com.avaliacao.service.ClienteService;
import br.com.avaliacao.dto.ClienteDTO;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


@RestController
@RequestMapping("/cliente")
public class ClienteController  {

    @Autowired
    private ClienteService service;
    private ClienteMapper mapper=new ClienteMapper();;


    @GetMapping
    @ApiOperation(value = "List all")
    @DefaultParamsPagedList
     Page<ClienteDTO> getAll(Integer page, Integer count, String order, String sortProperty, ClienteDTOFilter clienteDTOFilter) {
        PageRequest pageRequest = PagingUtils.createPageRequest(page, count, order, sortProperty);
        Page<ClienteDTO> pages = service.listarTodosPaginado(pageRequest,clienteDTOFilter);
        return pages;
    }

    @GetMapping("/findById")
    public ClienteDTO findById(@RequestParam( "clientId") Long  clientId) {

        return mapper.convertToDTO(service.get(clientId));
    }

    @PostMapping
    @ApiOperation("insert cliente")
    public ClienteDTO create(@ApiParam(value = "clienteDTO", required = true)
                                                 @RequestBody @Valid ClienteDTO clienteDTO) {
        Cliente cliente = mapper.convertToEntity(clienteDTO);
        return this.mapper.convertToDTO(this.service.add(cliente));
    }

    @PutMapping()
    @ApiOperation("Atualizar Cliente")
    public ClienteDTO update(@ApiParam(value = "ClientDTO", required = true)
                                  @RequestBody @Valid  ClienteDTO clientDTO) {
        Cliente cliente =mapper.convertToEntity(clientDTO);
        return this.mapper.convertToDTO(this.service.update(cliente));
    }

    @DeleteMapping("/{id}")
    @ApiOperation("Delete cliente")
    public void delete(@ApiParam(value = "id", required = true) @PathVariable  Long id) {
        service.remove(id);
    }
}
