package br.com.avaliacao.controller.exception;


import lombok.Getter;
@Getter
public class AvlException extends RuntimeException {

    private final String[] args;

    public AvlException(String message, String... args) {
        super(message);
        this.args = args;
    }

    public AvlException(String message, Throwable cause, String... args) {
        super(message, cause);
        this.args = args;
    }
}