package br.com.avaliacao.controller.exception;

public class InvalidValueException extends AvlException {
    private static final long serialVersionUID = 1L;

    public InvalidValueException(String message, String... args) {
        super(message, args);
    }

    public InvalidValueException(String message, Throwable cause, String... args) {
        super(message, cause, args);
    }
}
