package br.com.avaliacao.controller.exception;

public  class NotFoundException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public NotFoundException(String message, String... args) {
        super(message);
    }

    public NotFoundException(String message, Throwable cause, String... args) {
        super(message, cause);
    }
}
