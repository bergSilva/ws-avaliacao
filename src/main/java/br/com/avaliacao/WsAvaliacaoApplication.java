package br.com.avaliacao;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WsAvaliacaoApplication {

	public static void main(String[] args) {
		SpringApplication.run(WsAvaliacaoApplication.class, args);
	}

}
