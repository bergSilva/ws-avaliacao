package br.com.avaliacao.util;

import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public final class DateUtils {

    private DateUtils() {
    }

    public static Date convertStringDDMMYYYYToDate(String stringDate) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        return sdf.parse(stringDate);
    }

    public static Integer getIdade(Date datanasce){
        GregorianCalendar hj=new GregorianCalendar();
        GregorianCalendar nascimento=new GregorianCalendar();
        if(datanasce!=null){
            nascimento.setTime(datanasce);
        }
        int anohj=hj.get(Calendar.YEAR);
        int anoNascimento=nascimento.get(Calendar.YEAR);
        return new Integer(anohj-anoNascimento);
    }

    public static String converteDateToString(Date date){
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        return sdf.format(date);
    }

}
