package br.com.avaliacao.mapper;



import br.com.avaliacao.controller.exception.AvlException;
import br.com.avaliacao.dto.ClienteDTO;
import br.com.avaliacao.dto.custom.CidadeCustomDTO;
import br.com.avaliacao.dto.filter.ClienteDTOFilter;
import br.com.avaliacao.model.Cidade;
import br.com.avaliacao.model.Cliente;
import br.com.avaliacao.util.DateUtils;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

public class ClienteMapper {

    public static List<ClienteDTO> entityToListDTO(List<Cliente> clients) {
         List<ClienteDTO> clientDTOS = new ArrayList<>();
        clients.forEach(cliente -> {
            clientDTOS.add(ClienteDTO.builder()
                     .id(cliente.getId())
                     .nomeCompleto(cliente.getNomeCompleto())
                     .sexo(cliente.getSexo())
                    .dataNasc(DateUtils.converteDateToString(cliente.getDataNasc()))
                    .idade(DateUtils.getIdade(cliente.getDataNasc()))
                     .build());
         });
        return clientDTOS;
     }

    public  static ClienteDTO convertToDTO(Cliente entity) {

        return ClienteDTO.builder()
                .id(entity.getId())
                .nomeCompleto(entity.getNomeCompleto())
                .sexo(entity.getSexo())
                .dataNasc(DateUtils.converteDateToString(entity.getDataNasc()))
                .idade(DateUtils.getIdade(entity.getDataNasc()))
                .cidade(CidadeCustomDTO.builder()
                        .id(entity.getCidade().getId())
                        .nome(entity.getCidade().getNome())
                        .build())
                .build();
    }

    public static Cliente convertToEntity(ClienteDTO dto) {
        try {
            return Cliente.builder()
                    .id(dto.getId())
                    .nomeCompleto(dto.getNomeCompleto())
                    .sexo(dto.getSexo())
                    .dataNasc(DateUtils.convertStringDDMMYYYYToDate(dto.getDataNasc()))
                    .cidade(Cidade.builder().id(dto.getCidade().getId()).build())
                    .build();
        } catch (ParseException e) {
            throw  new AvlException("Erro na coversao de dados");
        }
    }

    public static Cliente convertFilterToEntity(ClienteDTOFilter filterDTO) {
        return Cliente.builder()
                .id(filterDTO.getId())
                .nomeCompleto(filterDTO.getNomeCompleto())
                .build();
    }




}
