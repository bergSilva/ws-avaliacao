package br.com.avaliacao.mapper;



import br.com.avaliacao.dto.CidadeDTO;
import br.com.avaliacao.dto.filter.CidadeDTOFilter;
import br.com.avaliacao.model.Cidade;
import br.com.avaliacao.model.Estado;

public class CidadeMapper  {


    public static Cidade convertFilterToEntity(CidadeDTOFilter filterDTO) {
        return Cidade.builder()
                .nome(filterDTO.getNome())
                .estado(Estado.builder().nome(filterDTO.getEstadoNome()).build()).build();
    }


    public static CidadeDTO convertToDTO(Cidade entity) {
        return CidadeDTO.builder()
                .id(entity.getId())
                .nome(entity.getNome())
                .estado(entity.getEstado()).build();
    }


    public static Cidade convertToEntity(CidadeDTO dto) {
        return Cidade.builder()
                .id(dto.getId())
                .nome(dto.getNome())
                .estado(dto.getEstado()).build();
    }
}
