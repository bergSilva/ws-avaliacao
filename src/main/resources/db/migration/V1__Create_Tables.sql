CREATE TABLE IF NOT EXISTS `avaliacaodb`.`estado` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(255) NULL DEFAULT NULL,
  `sigla` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id`));

  CREATE TABLE IF NOT EXISTS `avaliacaodb`.`cidade` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(255) NULL DEFAULT NULL,
  `estado_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  foreign key (`estado_id` ) references estado (`id`)  );

  CREATE TABLE IF NOT EXISTS `avaliacaodb`.`cliente` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `data_nasc` DATE NULL DEFAULT NULL,
  `nome_completo` VARCHAR(255) NULL DEFAULT NULL,
  `sexo` INT(11) NULL DEFAULT NULL,
  `cidade_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  foreign key (`cidade_id`) references cidade (`id`));