package br.com.avaliacao.util;

import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


import static org.junit.jupiter.api.Assertions.assertEquals;

public class DateUtilsTeste {

    @Test
    void convertStringDDMMYYYYToDate() throws ParseException {
        String  data = "20/06/1989";
        Date dateConvertido = DateUtils.convertStringDDMMYYYYToDate(data);
        Date dateAConverter = new SimpleDateFormat("dd/MM/yyyy").parse("20/06/1989");
        assertEquals(dateAConverter,dateConvertido);
    }


    @Test
    void getIdade() throws ParseException {
        Date datenasce = new SimpleDateFormat("dd/MM/yyyy").parse("20/06/1989");
        int idade = DateUtils.getIdade(datenasce);
        assertEquals(30, idade);
    }

    @Test
    void converteDateToString() throws ParseException {
        Calendar calendar = Calendar.getInstance();
        Date hoje = calendar.getTime();
        String dataConvertida = DateUtils.converteDateToString(hoje);
        assertEquals("15/08/2019", dataConvertida);
    }


}
